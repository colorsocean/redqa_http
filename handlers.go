package redqa_http

import (
	"encoding/json"
	"log"
	"net/http"
	"time"

	"bitbucket.org/colorsocean/redqa"
	"bitbucket.org/colorsocean/response"
	"bitbucket.org/colorsocean/rtool"
)

type RedQAHTTP struct {
	//> App-context
	RedQA    *redqa.RedQA
	PageSize int

	//> Request-context
	UserID      string
	UserName    string
	UserIsAdmin bool

	Response response.Response
}

func (this RedQAHTTP) NewDialog(w http.ResponseWriter, q *http.Request) {
	var req struct { //> REQUEST
		Message  string
		Category string
	}

	var resp struct {
		Dialog  *redqa.Dialog
		Message *redqa.Message
	}

	_, err := rtool.Read(rtool.Options{Q: q, Out: &req, Response: this.Response, Validate: true})
	if err != nil {
		panic(err)
	}

	resp.Dialog, resp.Message = this.RedQA.StartDialog(
		this.UserID,
		this.UserName,
		req.Message,
		req.Category,
	)

	this.Response.Payload(resp)
}

func (this RedQAHTTP) SendMessage(w http.ResponseWriter, q *http.Request) {
	var req struct { //> REQUEST
		DialogID int
		Message  string
	}

	var resp struct {
		Message *redqa.Message
	}

	_, err := rtool.Read(rtool.Options{Q: q, Out: &req, Response: this.Response, Validate: true})
	if err != nil {
		panic(err)
	}

	dialog := this.RedQA.Dialog(req.DialogID)
	resp.Message = dialog.AddMessage(
		req.Message,
		dialog.Info().UserID,
		this.UserName,
		this.UserIsAdmin,
	)

	this.Response.Payload(resp)
}

func (this RedQAHTTP) CloseDialog(w http.ResponseWriter, q *http.Request) {
	var req struct { //> REQUEST
		DialogID int
	}

	var resp struct {
	}

	_, err := rtool.Read(rtool.Options{Q: q, Out: &req, Response: this.Response, Validate: true})
	if err != nil {
		panic(err)
	}

	this.RedQA.Dialog(req.DialogID).Close()

	this.Response.Payload(resp)
}

func (this RedQAHTTP) GetDialog(w http.ResponseWriter, q *http.Request) {
	var req struct { //> REQUEST
		DialogID int
		Page     int
	}

	var resp struct {
		Dialog   *redqa.Dialog
		Messages []*redqa.Message
	}

	_, err := rtool.Read(rtool.Options{Q: q, Out: &req, Response: this.Response, Validate: true})
	if err != nil {
		panic(err)
	}

	if req.Page < 1 {
		req.Page = 1
	}

	dialog := this.RedQA.Dialog(req.DialogID)
	resp.Dialog = dialog.Info()
	resp.Messages = dialog.Messages(((req.Page - 1) * this.PageSize), this.PageSize)

	for _, msg := range resp.Messages {
		log.Println("MSG:", msg.ID, msg.ByAdmin, msg.Read)
		if !msg.Read && ((msg.ByAdmin && !this.UserIsAdmin) || (!msg.ByAdmin && this.UserIsAdmin)) {
			log.Println("MSG MARKING:", msg.ID, msg.ByAdmin, msg.Read)
			this.RedQA.MarkRead(*msg)
		}
	}

	this.Response.Payload(resp)
}

func (this RedQAHTTP) GetDialogsWithMessages(w http.ResponseWriter, q *http.Request) {
	var req struct { //> REQUEST
		Page int
	}

	type DialogData struct {
		Dialog   *redqa.Dialog
		Messages []*redqa.Message
	}

	var resp struct {
		Dialogs []DialogData
	}

	_, err := rtool.Read(rtool.Options{Q: q, Out: &req, Response: this.Response, Validate: true})
	if err != nil {
		panic(err)
	}

	if req.Page < 1 {
		req.Page = 1
	}

	var dialogs []*redqa.DialogModel
	if this.UserIsAdmin {
		dialogs = this.RedQA.Dialogs(((req.Page - 1) * this.PageSize), this.PageSize)
	} else {
		dialogs = this.RedQA.DialogsByUser(this.UserID, ((req.Page - 1) * this.PageSize), this.PageSize)
	}

	for _, dialog := range dialogs {
		dd := DialogData{}
		dd.Dialog = dialog.Info()
		dd.Messages = dialog.Messages(((req.Page - 1) * this.PageSize), this.PageSize)

		resp.Dialogs = append(resp.Dialogs, dd)
	}

	this.Response.Payload(resp)
}

func (this RedQAHTTP) Pusher(w http.ResponseWriter, q *http.Request) {
	log.Println("[pusher] started", this.UserID, this.UserIsAdmin)
	respond := func(v interface{}) {
		w.Header().Set("Content-Type", "application/json")
		data, err := json.MarshalIndent(v, "", "  ")
		if err != nil {
			panic(data)
		}
		w.Write(data)
	}

	events, ts := this.RedQA.SyncEvents(this.UserID, this.UserIsAdmin)
	if len(events) != 0 {
		log.Println("[pusher] got some events")
		respond(events)
		this.RedQA.SetSynced(this.UserID, this.UserIsAdmin, ts)
		return
	}

	log.Println("[pusher] no events")

	unsubscribe, recv := this.RedQA.Subscribe(this.UserID, this.UserIsAdmin)
	defer unsubscribe()

	log.Println("[pusher] subscribed to channel")

L:
	for {
		select {
		case msg := <-recv:
			log.Println("[pusher] received message:", msg)

			events, ts := this.RedQA.SyncEvents(this.UserID, this.UserIsAdmin)
			if len(events) != 0 {
				log.Println("[pusher] YAY, GOT NEW EVENTS!!!!!")
				respond(events)
				this.RedQA.SetSynced(this.UserID, this.UserIsAdmin, ts)
				break L
			} else {
			}
		case <-time.After(60 * time.Second):
			log.Println("[pusher] timed out")
			respond([]interface{}{})
		}
	}
	log.Println("[pusher] quit")
}
